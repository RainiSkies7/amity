/*
*    This file is part of Amity.
*
*   Amity is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   Amity is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with Amity.  If not, see <http://www.gnu.org/licenses/>.
*/

"use strict";

var config = require('../config.json');

function addon(event, client) {
        if (event.args.length >= 1) {
            event.replyAction('tosses ' + event.args[0] +' a bomb...');
            event.channel.kick(event.args[0], 'KABOOM!', event.network);
        } else {
            event.replyAction('tosses ' + event.user.nick + ' a bomb...');
            event.channel.kick(event.user, 'KABOOM!', event.network);
        }
}

module.exports = addon;
