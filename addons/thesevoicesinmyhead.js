/*
*    This file is part of Amity.
*
*   Amity is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   Amity is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with Amity.  If not, see <http://www.gnu.org/licenses/>.
*/

"use strict";

function addon(event, client) {
    var voices = ['WHAT ARE YOU CONCERNED ABOUT ', 'RAIN ', 'ARE ', 'AND ', 'VOICES ', 'SO ARE THE ', 'SO ARE ', 'VOICES IN MY HEAD ',
        'SEND HELP ', 'BACK ', 'FRONT ', 'NO ', 'YES ', 'MELODY ', 'AREN\'T YOU ALONE ', 'SAVE ME ', 'VIOLET ', 'MUFFIN ', 'RAINI ', 'ALONE ',
        'SO ALONE ', 'HELLO ', 'MUSIC ', 'WOULD YOU ', 'PLEASE ', 'I CAN\'T FEEL ', 'GO AWAY ', 'IS IT WRONG ', 'IF ONLY I COULD ', 'GOODBYE '];
    var thevoice = voices[Math.floor(Math.random() * voices.length)] + voices[Math.floor(Math.random() * voices.length)] + voices[Math.floor(Math.random() * voices.length)] + voices[Math.floor(Math.random() * voices.length)] + voices[Math.floor(Math.random() * voices.length)];
    event.reply(thevoice);
}

module.exports = addon;
