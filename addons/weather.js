/*
*    This file is part of Amity.
*
*   Amity is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   Amity is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with Amity.  If not, see <http://www.gnu.org/licenses/>.
*/

"use strict";

var config = require("../config.json");
var Forecast = require("forecast.io");
var geocoder = require('geocoder');

var options = {
  APIKey: config.weatherapi,
  timeout: 1000
},
forecast = new Forecast(options);

function addon(event, client) {
    if (event.args < 1){
	client.say(to, "No location specified");
    } else {
	try {
	    var locdata = null;
	    geocoder.geocode(event.args.toString(), function(err, data) {
		locdata = data;
		if (data.status === 'ZERO_RESULTS') {
		    console.log(err);
		    event.reply('No results for location');
		    return;
		}
	    });
	    setTimeout(function() {
	        console.log(locdata);
		try {
	            forecast.get(locdata.results[0].geometry.location.lat, locdata.results[0].geometry.location.lng, function (err, res, data) {
  		        if (err) {
			    console.log(err);
		        } else {
		    	    event.reply("Weather for " + locdata.results[0].formatted_address + ": " + data.currently.temperature + "°F/" + ((data.currently.temperature - 32) * .5556).toFixed(2) + "°C - "  + data.currently.summary);
		        }
	            });
		} catch (e) {
		    console.log(e);
		}
	    }, 1000);
	} catch (e) {
	    console.log(e);
	}
    }
}

module.exports = addon;
