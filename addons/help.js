/*
*    This file is part of Amity.
*
*   Amity is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   Amity is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with Amity.  If not, see <http://www.gnu.org/licenses/>.
*/

"use strict";

var fs = require('fs');

function addon(event, client) {
    var files = fs.readdirSync('addons/');
    var commands = '';
    for (var i = 0; i < files.length; i++) {
        if (i + 1 === files.length) {
            commands = commands.concat(files[i].replace('.js', ''));
        } else {
            commands = commands.concat(files[i].replace('.js', '') + ', ');
        }
    }
    event.reply('Commands available: ' + commands);
    event.reply('Additionally, I can parse URLS and YouTube links. I also do some more stuff!');
}

module.exports = addon;
