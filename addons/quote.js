/*
*    This file is part of Amity.
*
*   Amity is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   Amity is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with Amity.  If not, see <http://www.gnu.org/licenses/>.
*/

"use strict";

var fs = require('fs');
var file = "quote.db";
var exists = fs.existsSync(file);
var sqlite3 = require("sqlite3").verbose();
var db = new sqlite3.Database(file);

function addquote(database, quote) {
    db.serialize(function() {
	db.run("CREATE TABLE if not exists " + database + " (id integer primary key, quote text)");
	db.run("INSERT INTO " + database + "(quote) values(\'" + quote + "\')");
    });
}

function grabquote(event, database, id) {
    db.each("SELECT * FROM " + database + " WHERE id=" + id, function(err, row) {
	if (err) {
	    event.reply('Error! Probably a non-existant database');
	    console.log(err);
	} else {
	    var thequote = row.quote.split('_n');
	    console.log(thequote);
	    for (var i = 0; i < thequote.length; i++) {
    	        event.reply("<" + row.id + "> " + thequote[i]);
	    }
	}
    });
}

function addon(event, client) {
    try {
	console.log(event.args.toString());
	if (event.args[0] === 'list') {
	    db.all("SELECT name FROM sqlite_master WHERE type=\'table\'", function(err, row) {
		if (err) {
		    console.log(err);
		    event.reply('Error!');
		} else {
		    console.log(row);
		    console.log(row.length);
		    console.log(row[0].name);
		    var databaselist = 'Databases: ';
		    for (var i = 0; i < row.length; i++) {
			var databaselist = databaselist.concat(row[i].name + ', ');
		    }
		    event.reply(databaselist);
		}
	    });
	} else if (event.args.length === 1) {
	    db.all("SELECT id FROM " + event.args[0] + " ORDER BY RANDOM() LIMIT 1", function(err, row) {
		if (err) {
		    console.log(err);
		    event.reply('Error! Probably a non-existant database');
		} else {
		    console.log(row);
		    grabquote(event, event.args[0], row[0].id);
		}
	    });
	} else if (event.args.length >= 2) {
	    grabquote(event, event.args[0], event.args[1]);
	} else {
	    db.all('SELECT name FROM sqlite_master WHERE type=\'table\' ORDER BY RANDOM() LIMIT 1', function(err, row) {
		if (err) {
		    console.log(err);
		    event.reply('Error!');
		} else {
		    db.all("SELECT id FROM " + row[0].name + " ORDER BY RANDOM() LIMIT 1", function(err, row2) {
			if (err) {
			    console.log(err);
			    event.reply('Error!');
			} else {
			    grabquote(event, row[0].name, row2[0].id);
			}
		    });
		}
	    });
	}
    } catch (e) {
        console.log('We have an error');
        console.log(e);
    }
}

module.exports = addon;
