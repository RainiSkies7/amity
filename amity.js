/*
*    This file is part of Amity.
*
*   Amity is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   Amity is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with Amity.  If not, see <http://www.gnu.org/licenses/>.
*/

'use strict';

var config = require("./config.json");

var argv = require('yargs').argv;
if (argv.d || argv.debug) {
	config.debug = true;
}

if (config.debug === true) {
	var chans = config.debugchans;
} else {
	var chans = config.channels;
}
var loggedin = new Array;
var client = require("coffea")({
	host: config.server,
	port: config.port,
	ssl: config.secure,
	ssl_allow_invalid: config.selfSigned,
	prefix: config.command,
	channels: chans,
	nick: config.nick,
	username: config.userName,
	realname: config.realName,
	nickserv: {
		username: config.userName,
		password: config.password
	},
	throttling: config.floodProtectionDelay
});
var c = require("irc-colors");
var fs = require('fs');
var ytdl = require('youtube-dl');
var request = require("request");
var cheerio = require("cheerio");

console.log("Logging in as " + config.nick);



function addCommas(x) {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

client.on('join', function(event) {
	if (config.debug === true) {
		event.reply('Debug Mode');
	}
});

client.on('message', function(event) {
	console.log(event.channel.name + ' | <' + event.user.nick + '> ' + event.message);
	if (event.message.toLowerCase().indexOf('hi ' + config.nick.toLowerCase()) > -1) {
		event.reply('Hi!');
	}
	if (event.message.indexOf('WAKE ME UP') > -1) {
		event.reply('WAKE ME UP INSIDE');
	}
	if (event.message.indexOf('I CAN\'T WAKE UP') > -1) {
		event.reply('WAKE ME UP INSIDE');
	}
	if (event.message.indexOf('SAVE ME') > -1) {
		event.reply('CALL MY NAME AND SAVE ME FROM THE DARK');
	}
	var urlreg = /(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
	var urls = event.message.match(urlreg);
	if (urls) {
		console.log(urls);
		ytdl.getInfo(urls[0], function(err, info) {
			if (err) {
				console.log('probably doesn\'t exist')
			} else {
				console.log('Info recieved! ' + urls[0]);
				var likes =  c.green("+" + addCommas(info.like_count));
				var dislikes = c.red("-" + addCommas(info.dislike_count));
				event.reply(c.bglightgray("You") + c.white.bgred("Tube") + ": \"" + info.title + "\" - Views: " + addCommas(info.view_count) + " - Likes/Dislikes: " + likes + " / " + dislikes );
			}
		});
	}
	var urlreg0 = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
	var re = /(<\s*title[^>]*>(.+?)<\s*\/\s*title)>/gi;
	var url0 = event.message.match(urlreg0);
	if (url0 && !urls) {
		console.log(url0);
		request.get(url0[0], function (error, response, body) {
			if (!error && response.statusCode == 200) {
				var $ = cheerio.load(body);
				var title = $("title").text().replace(/\r?\n|\r/g, '').trim();
				console.log(title);
				if (title != "") {
					event.reply("URL: " + title)
				}
			};
		});
	}
});

client.on('privatemessage', function (event) {
	if (event.user.nick.indexOf(config.admin) > -1) {
		if (event.message === config.adminpass) {
			console.log(loggedin);
			event.reply('You are now logged in');
			loggedin.push(event.user.nick);
			console.log(loggedin);
		} else {
			event.reply('Incorrect password attempt');
		}
	}
});

client.on('command', function (event) {
	if (fs.existsSync('./addons/' + event.cmd + '.js')) {
		if (event.user.nick.indexOf(loggedin) > -1) {
			var command = require('./addons/' + event.cmd + '.js');
			command(event, client);
		}
	} else if (fs.existsSync('./admin/' + event.cmd + '.js')) {
		if (loggedin.indexOf(event.user.nick) > -1) {
			var command = require('./admin/' + event.cmd + '.js');
			command(event, client);
		} else {
			event.reply('Disabled Command');
		}
	}
	console.log('<' + event.user.nick + '> ' + event.message);
});
